<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class EmployeeController extends Controller
{
    public function create()
    {
        return view('admin.employee.form-add-employee');
    }

    public function insert(Request $request){
        $employees = new User;
        $employees->nom = $request->input('nom');
        $employees->prenom = $request->input('prenom');
        $employees->sexe = $request->input('sexe');
        $employees->email = $request->input('email');
        $employees->date_naiss = $request->input('date_naiss');
        $employees->lieu_naiss = $request->input('lieu_naiss');
        $employees->date_rec = $request->input('date_rec');
        $employees->lieu_rec = $request->input('lieu_rec');
        $employees->address = $request->input('address');
        $employees->tel1 = $request->input('tel1');
        $employees->tel2 = $request->input('tel2');
        $employees->intitule_fct = $request->input('intitule_fct');
        $employees->ech = $request->input('ech');
        $employees->cat = $request->input('cat');
        $employees->password = $request->input('password');
        $employees->save();
        return redirect()->route('admin.employee.employee-list')->with('status','User Updated Successfully');
    }

    public function edit($id)
    {
        $employees = User::find($id);
        return view('admin.employee.form-edit-employee', compact('employees'));
    }

    public function update(Request $request, $id)
    {
        $employees = User::find($id);
        $employees->nom = $request->input('nom');
        $employees->prenom = $request->input('prenom');
        $employees->sexe = $request->input('sexe');
        $employees->email = $request->input('email');
        $employees->date_naiss = $request->input('date_naiss');
        $employees->lieu_naiss = $request->input('lieu_naiss');
        $employees->date_rec = $request->input('date_rec');
        $employees->lieu_rec = $request->input('lieu_rec');
        $employees->address = $request->input('address');
        $employees->tel1 = $request->input('tel1');
        $employees->tel2 = $request->input('tel2');
        $employees->intitule_fct = $request->input('intitule_fct');
        $employees->ech = $request->input('ech');
        $employees->cat = $request->input('cat');
        $employees->update();
        return redirect()->route('admin.employee.employee-list')->with('status','User Updated Successfully');
    }

    public function destroy($id)
    {
        $employees = User::find($id);
        $employees->delete();
        return redirect()->back()->withErrors('Successfully deleted!');
    }
}

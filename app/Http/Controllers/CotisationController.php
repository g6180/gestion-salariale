<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cotisation;

class CotisationController extends Controller
{
    public function create()
    {
        return view('admin.cotisation.form-add-cotisation');
    }

    public function store(Request $request){
       $cotisations = new Cotisation;
       $cotisations->ech = $request->input('ech');
       $cotisations->cat = $request->input('cat');
       $cotisations->montant = $request->input('montant');
       $cotisations->desc = $request->input('desc');
       $cotisations->save();
        return redirect()->route('admin.cotisation.cotisation-list')->with('status','Ajoute avec succes dans grille de cotisation');
    }

    public function edit($id)
    {
        $cotisations = Cotisation::find($id);
        return view('admin.cotisation.form-edit-cotisation', compact('cotisations'));
    }

    public function update(Request $request, $id)
    {
        $cotisations = Cotisation::find($id);
        $cotisations->ech = $request->input('ech');
        $cotisations->cat = $request->input('cat');
        $cotisations->montant = $request->input('montant');
        $cotisations-> desc = $request->input(' desc');
        $cotisations->update();
        return redirect()->route('admin.cotisation.cotisation-list')->with('status','Modifier avec succes');
    }

    public function destroy($id)
    {
        $cotisations = Cotisation::find($id);
        $cotisations->delete();
        return redirect()->route('admin.cotisation.cotisation-list')->with('status','Supprimer avec succes');
    }
}

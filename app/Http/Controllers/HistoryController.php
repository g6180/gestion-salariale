<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Salarier;
use App\Models\history;
use PDF;

class HistoryController extends Controller
{

    public function payType($id){
        $employees = Salarier::find($id);
        return view('admin.pay.pay-type', compact('employees'));
    }

    public function normalePay($id)
    {
        $employees = Salarier::find($id);
        return view('admin.pay.normale-form-pay', compact('employees'));
    }

    public function advancePay($id)
    {
        $employees = Salarier::find($id);
        return view('admin.pay.advance-form-pay', compact('employees'));
    }

    public function PayStore(Request $request)
    {
        $history = new history;
        $history->users_id = $request->input('users_id');
        $history->type = $request->input('type');
        $history->mode = $request->input('mode');
        $history->operateur = $request->input('operateur');
        $history->num = $request->input('num');
        $history->IDTrans = $request->input('IDTrans');
        $history->montant = $request->input('montant');
        $history->date = $request->input('date');
        $history->save();
        return redirect()->route('admin.pay.pay-history')->with('status','Salarier payer avec succes');
        
    }

    public function index() {
        $employees = Salarier::get();
        $employees = DB::table('salariers')
            ->join('histories', 'salariers.id', '=', 'histories.users_id')
            ->select('salariers.*', 'histories.*')
            ->get();
        $history = history::paginate(9);
        return view('admin.pay.pay-history', compact('employees', 'history'));
    }

    public function search(Request $request)
    {
        $key = trim($request->get('q'));

        $employees = history::all();
        $employees = history::query()
            // ->where('nom', 'like', "%{$key}%")
            // ->orWhere('prenom', 'like', "%{$key}%")
            // ->orWhere('sexe', 'like', "%{$key}%")
            // ->orWhere('email', 'like', "%{$key}%")
            // ->orWhere('tel', 'like', "%{$key}%")
            // ->orWhere('intitule_fct', 'like', "%{$key}%")
            ->where('mode', 'like', "%{$key}%")
            ->orWhere('operateur', 'like', "%{$key}%")
            ->orWhere('num', 'like', "%{$key}%")
            ->orWhere('IDTrans', 'like', "%{$key}%")
            ->orWhere('montant', 'like', "%{$key}%")
            ->orWhere('date', 'like', "%{$key}%")
            ->orderBy('created_at', 'desc')
            ->get();

        return view('admin.pay.pay-search', [
            'key' => $key,
            'employees' => $employees
        ]);
    }

    public function payinvoice($id,$date)
    {
        $employees = Salarier::find($id);
        $salariers = Salarier::find($date);
        $salariers = DB::table('salariers')
            ->join('histories','salariers.id','=','histories.users_id')
            ->select('salariers.*','histories.*')
            ->where('salariers.id',$id)
            ->where('histories.date',$date)
            ->get();
        // $historiques = DB::select('select * from histories where users_id = ?', [1]);
        return view('admin.pay.pay-invoice', compact('employees','salariers'));
    }

    public function employeesPayHistoryList($id){
        $employees = Salarier::find($id);
        $employees = Salarier::get();
        $historiques = DB::select('select * from histories where users_id = ?', [$id]);
        $employees = Salarier::paginate(19);
        return view('admin.pay.employeesPayHistoryList', compact('employees', 'historiques'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $employees = Salarier::find($id);
        $Hystory = history::get();
        return view('admin.pay.form-pay', compact('employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $history = new history;
        $history->users_id = $request->input('users_id');
        $history->mode = $request->input('mode');
        $history->operateur = $request->input('operateur');
        $history->num = $request->input('num');
        $history->IDTrans = $request->input('IDTrans');
        $history->montant = $request->input('montant');
        $history->date = $request->input('date');
        $history->save();
        return redirect()->route('admin.pay.pay-history')->with('status','Salarier payer avec succes');
        
    }

   

    public function generatePayInvoiceToPdf($id,$date)
    {
        $employees = Salarier::find($id);
        $salariers = Salarier::find($date);
        $salariers = DB::table('salariers')
            ->join('histories','salariers.id','=','histories.users_id')
            ->select('salariers.*','histories.*')
            ->where('salariers.id',$id)
            ->where('histories.date',$date)
            ->get();
        return PDF::loadView('admin.pay.generatePayInvoiceToPdf', compact('employees', 'salariers'))
            ->setPaper('a4', 'landscape')
            ->setWarnings(false)
            // ->save(public_path("storage/documents/fichier.pdf"))
            ->stream();
    }
}

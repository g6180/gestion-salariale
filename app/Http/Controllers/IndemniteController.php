<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Indemnite;

class IndemniteController extends Controller
{
    public function create()
    {
        return view('admin.indemnite.form-add-indemnite');
    }

    public function store(Request $request){
       $indemnites = new Indemnite;
       $indemnites->ech = $request->input('ech');
       $indemnites->cat = $request->input('cat');
       $indemnites->montant = $request->input('montant');
       $indemnites->desc = $request->input('desc');
       $indemnites->save();
        return redirect()->route('admin.indemnite.indemnite-list')->with('status','Ajoute avec succes dans grille la indemnitaire salariale');
    }

    public function edit($id)
    {
        $indemnites = Indemnite::find($id);
        return view('admin.indemnite.form-edit-indemnite', compact('indemnites'));
    }

    public function update(Request $request, $id)
    {
        $indemnites = Indemnite::find($id);
        $indemnites->ech = $request->input('ech');
        $indemnites->cat = $request->input('cat');
        $indemnites->montant = $request->input('montant');
        $indemnites-> desc = $request->input(' desc');
        $indemnites->update();
        return redirect()->route('admin.indemnite.indemnite-list')->with('status','Modifier avec succes');
    }

    public function destroy($id)
    {
        $indemnites = Indemnite::find($id);
        $indemnites->delete();
        return redirect()->route('admin.indemnite.indemnite-list')->with('status','Supprimer avec succes');
    }
}

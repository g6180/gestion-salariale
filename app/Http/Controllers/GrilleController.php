<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Grille;

class GrilleController extends Controller
{
    public function create()
    {
        return view('admin.grille.form-add-grille');
    }

    public function store(Request $request){
       $grilles = new Grille;
       $grilles->ech = $request->input('ech');
       $grilles->cat = $request->input('cat');
       $grilles->salbas = $request->input('salbas');
       $grilles->poste = $request->input('poste');
       $grilles->save();
        return redirect()->route('admin.grille.salary-grille-list')->with('status','Ajoute avec succes dans la grille salariale');
    }

    public function edit($id)
    {
        $grilles = Grille::find($id);
        return view('admin.grille.form-edit-grille', compact('grilles'));
    }

    public function update(Request $request, $id)
    {
        $grilles = Grille::find($id);
        $grilles->ech = $request->input('ech');
        $grilles->cat = $request->input('cat');
        $grilles->salbas = $request->input('salbas');
        $grilles-> poste = $request->input(' poste');
        $grilles->update();
        return redirect()->route('aadmin.grille.salary-grille-list')->with('status','User Updated Successfully');
    }

    public function destroy($id)
    {
        $grilles = Grille::find($id);
        $grilles->delete();
        return redirect()->back()->withErrors('Successfully deleted!');
    }
}

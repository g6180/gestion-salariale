<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CatSal;

class CatSalController extends Controller
{
    public function create()
    {
        return view('admin.catsal.form-add-catsal');
    }

    public function store(Request $request){
       $catsalaires = new CatSal;
       $catsalaires->cat = $request->input('cat');
       $catsalaires->salbas = $request->input('salbas');
       $catsalaires->save();
        return redirect()->route('admin.catsal.catsal-list')->with('status','Ajoute avec succes dans la grille salariale par salariale');
    }

    public function edit($id)
    {
        $catsalaires = CatSal::find($id);
        return view('admin.catsal.form-edit-catsal', compact('catsalaires'));
    }

    public function update(Request $request, $id)
    {
        $catsalaires = CatSal::find($id);
        $catsalaires->cat = $request->input('cat');
        $catsalaires->salbas = $request->input('salbas');
        $catsalaires->update();
        return redirect()->route('admin.catsal.catsal-list')->with('status','Modifier avec succes');
    }

    public function destroy($id)
    {
        $catsalaires = CatSal::find($id);
        $catsalaires->delete();
        return redirect()->route('admin.catsal.catsal-list')->with('status','Supprimer avec succes');
    }
}

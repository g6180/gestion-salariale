<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EchSal;

class EchSalController extends Controller
{
    public function create()
    {
        return view('admin.echsal.form-add-echsal');
    }

    public function store(Request $request){
       $echsalaires = new EchSal;
       $echsalaires->ech = $request->input('ech');
       $echsalaires->salbas = $request->input('salbas');
       $echsalaires->save();
        return redirect()->route('admin.echsal.echsal-list')->with('status','Ajoute avec succes dans la grille salariale par echelons');
    }

    public function edit($id)
    {
        $echsalaires = EchSal::find($id);
        return view('admin.echsal.form-edit-echsal', compact('echsalaires'));
    }

    public function update(Request $request, $id)
    {
        $echsalaires = EchSal::find($id);
        $echsalaires->ech = $request->input('ech');
        $echsalaires->salbas = $request->input('salbas');
        $echsalaires->update();
        return redirect()->route('admin.echsal.echsal-list')->with('status','Modifier avec succes');
    }

    public function destroy($id)
    {
        $echsalaires = EchSal::find($id);
        $echsalaires->delete();
        return redirect()->route('admin.echsal.echsal-list')->with('status','Supprimer avec succes');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\history;
use App\Models\Grille;
use App\Models\Indemnite;
use App\Models\Cotisation;
use App\Http\Controllers\Fpdf;
use App\Models\CatSal;
use App\Models\EchSal;
use App\Models\Salarier;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = User::get();
        $employees = Salarier::get();
        $history = history::get();
        $grilles = Grille::get();
        $indemnites = Indemnite::get();
        $cotisations = Cotisation::get();
        $echsalaires = EchSal::get();
        $catsalaires = CatSal::get();
        return view('admin.acceuil', compact('admins', 'employees', 'history', 'grilles', 'indemnites', 'cotisations', 'echsalaires', 'catsalaires'));
    }

    public function showAdminList() {
        $admins = User::get();
        $admins = User::paginate(19);
        return view('admin.admin-list', compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admins = User::find($id);
        return view('admin.form-edit-admin', compact('admins'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admins = User::find($id);
        $admins->nom = $request->input('nom');
        $admins->prenom = $request->input('prenom');
        $admins->sexe = $request->input('sexe');
        $admins->email = $request->input('email');
        $admins->update();
        return redirect()->route('admin.admin-list')->with('status','Profil modifier avec sucses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admins = User::find($id);
        $admins->delete();
        return redirect()->route('admin.admin-list')->with('status','Administrateur supprimer avec succes');
    }
}

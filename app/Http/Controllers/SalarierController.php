<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Salarier;

class SalarierController extends Controller
{

    public function index()
    {
        $employees = Salarier::get();
        $employees = Salarier::paginate(9);
        return view('admin.employee.employee-list', compact('employees'));
    }

    public function search(Request $request)
    {
        $key = trim($request->get('q'));

        $salariers = Salarier::all();
        $salariers = Salarier::query()
            ->where('nom', 'like', "%{$key}%")
            ->orWhere('prenom', 'like', "%{$key}%")
            ->orWhere('sexe', 'like', "%{$key}%")
            ->orWhere('email', 'like', "%{$key}%")
            ->orWhere('tel', 'like', "%{$key}%")
            ->orWhere('intitule_fct', 'like', "%{$key}%")
            ->orderBy('created_at', 'desc')
            ->get();

        $categories = Salarier::all();
        $tags = Salarier::all();
        $recent_posts = Salarier::query()
            ->where('nom', true)
            ->orderBy('created_at', 'desc')
            ->take(5)
            ->get();

        return view('admin.employee.employee-search', [
            'key' => $key,
            'salariers' => $salariers,
            'categories' => $categories,
            'tags' => $tags,
            'recent_posts' => $recent_posts
        ]);
    }

    public function create()
    {
        return view('admin.employee.form-add-employee');
    }

    public function insert(Request $request){
        $employees = new Salarier;
        $employees->nom = $request->input('nom');
        $employees->prenom = $request->input('prenom');
        $employees->sexe = $request->input('sexe');
        $employees->email = $request->input('email');
        // $employees->date_naiss = $request->input('date_naiss');
        // $employees->lieu_naiss = $request->input('lieu_naiss');
        // $employees->date_rec = $request->input('date_rec');
        // $employees->lieu_rec = $request->input('lieu_rec');
        // $employees->address = $request->input('address');
        $employees->tel = $request->input('tel');
        $employees->nss = $request->input('nss');
        $employees->intitule_fct = $request->input('intitule_fct');
        // $employees->ech = $request->input('ech');
        // $employees->cat = $request->input('cat');
        $employees->save();
        return redirect()->route('admin.employee.employee-list')->with('status','Salarier Updated Successfully');
    }

    public function edit($id)
    {
        $employees = Salarier::find($id);
        return view('admin.employee.form-edit-employee', compact('employees'));
    }

    public function update(Request $request, $id)
    {
        $employees = Salarier::find($id);
        $employees->nom = $request->input('nom');
        $employees->prenom = $request->input('prenom');
        $employees->sexe = $request->input('sexe');
        $employees->email = $request->input('email');
        // $employees->date_naiss = $request->input('date_naiss');
        // $employees->lieu_naiss = $request->input('lieu_naiss');
        // $employees->date_rec = $request->input('date_rec');
        // $employees->lieu_rec = $request->input('lieu_rec');
        // $employees->address = $request->input('address');
        $employees->tel = $request->input('tel');
        $employees->nss = $request->input('nss');
        $employees->intitule_fct = $request->input('intitule_fct');
        // $employees->ech = $request->input('ech');
        // $employees->cat = $request->input('cat');
        $employees->update();
        return redirect()->route('admin.employee.employee-list')->with('status','Salarier Updated Successfully');
    }

    public function destroy($id)
    {
        $employees = Salarier::find($id);
        $employees->delete();
        return redirect()->back()->withErrors('Successfully deleted!');
    }
}

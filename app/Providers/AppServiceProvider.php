<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Paginator::useBootstrap();
    }

    public function scopeSearchInAvailableProperty($query, $search)
    {
        return $query->where(function ($q) use ($search) {
 
            $q->where('id', '=', (int)"{$search}")
                ->orWhere('email', 'like', "{$search}%")
                ->orWhere('last_name', 'like', "{$search}%")
                ->orWhere('first_name', 'like', "{$search}%");
        });
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Salarier extends Model
{
    use HasFactory;

    protected $fillable = [
        'nom',
        'prenom',
        'sexe',
        'email',
        'date_naiss',
        'lieu_naiss',
        'date_rec',
        'lieu_rec',
        'address',
        'tel1',
        'tel2',
        'intitule_fct',
        'ech',
        'cat',
        'password',
    ];
}

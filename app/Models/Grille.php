<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Grille extends Model
{
    use HasFactory;

    protected $fillable = [
        'ech',
        'cat',
        'salbas',
        'poste',
    ];
}

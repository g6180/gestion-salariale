<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class history extends Model
{
    use HasFactory;

    protected $fillable = [
        'users_id',
        'mode',
        'operateur',
        'num',
        'IDTrans',
        'montant',
        'date',
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Indemnite extends Model
{
    use HasFactory;

    protected $fillable = [
        'ech',
        'cat',
        'montant',
        'desc',
    ];
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $faker = Faker::create();
    	foreach (range(1,10) as $index) {
            DB::table('users')->insert([
                'nom' => $faker->nom,
                'prenom' => $faker->prenom,
                'sexe' => $faker->sexe,
                'email' => $faker->email,
                'email_verified_at' => $faker->email_verified_at,
                'date_naiss' => $faker->date_naiss,
                'lieu_naiss' => $faker->lieu_naiss,
                'date_rec' => $faker->date_rec,
                'lieu_rec' => $faker->lieu_rec,
                'address' => $faker->address,
                'tel1' => $faker->tel1,
                'tel2' => $faker->tel2,
                'intitule_fct' => $faker->intitule_fct,
                'ech' => $faker->ech,
                'cat' => $faker->cat,
                'type' => $faker->type,
                'password' => make::hash($faker->password),
                // 'phone_number' => $faker->phoneNumber,
                // 'dob' => $faker->date($format = 'D-m-y', $max = '2000',$min = '1990')
            ]);
        }
    }
}

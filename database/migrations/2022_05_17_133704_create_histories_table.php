<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('users_id')->unsigned()->nullable();
            // $table->foreign('users_id')->references('id')->on('users')->onupdate('restrict')->ondelete('restrict');
            $table->string('type')->default('Salaire Normale');
            $table->string('mode');
            $table->string('operateur');
            $table->string('num');
            $table->string('IDTrans')->unique();
            $table->string('montant')->nullable();
            $table->date('date');
            $table->string('solde')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('histories');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalariersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salariers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nom');
            $table->string('prenom');
            $table->string('sexe');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->date('date_naiss')->nullable();
            $table->string('lieu_naiss')->default('Non defini');
            $table->date('date_rec')->nullable();
            $table->string('lieu_rec')->default('Non defini');
            $table->string('address')->default('Non defini');
            $table->string('tel')->unique();
            $table->string('nss')->unique();
            $table->string('intitule_fct')->default('Agent');
            $table->string('ech')->default('1');
            $table->string('cat')->default('1');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salariers');
    }
}

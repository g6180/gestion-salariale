        @auth
        {{-- <nav class="navbar navbar-dark bg-dark" aria-label="First navbar example">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">Gestion salariale</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample01" aria-controls="navbarsExample01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample01">
                    <ul class="navbar-nav me-auto mb-2">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="{{ route('admin.acceuil') }}">Acceuil</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="{{ route('admin.employee.employee-list') }}">Liste des salariers</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="{{ route('admin.pay.pay-history') }}">Historique de paiement</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="{{ route('admin.grille.salary-grille-list') }}">Grille salariale</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="{{ route('admin.indemnite.indemnite-list') }}">Grille indemnitaire</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="{{ route('admin.cotisation.cotisation-list') }}">Grille de retenue ou de cotisation</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="{{ route('admin.echsal.echsal-list') }}">Grille salariale par echelons</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="{{ route('admin.catsal.catsal-list') }}">Grille salariale par categorie</a>
                        </li>
                        @guest
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-bs-toggle="dropdown" aria-expanded="false">Connexion</a>
                                <ul class="dropdown-menu" aria-labelledby="dropdown01">
                                    @if (Route::has('login'))
                                        <li><a class="dropdown-item"  href="{{ route('login') }}">{{ __('Connexion') }}</a></li>
                                    @endif
                                    @if (Route::has('register'))
                                        <li><a class="dropdown-item"  href="{{ route('register') }}">{{ __('Inscription') }}</a></li>
                                    @endif
                                </ul>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-bs-toggle="dropdown" aria-expanded="false">{{ Auth::user()->nom }} {{ Auth::user()->prenom }}</a>
                                <ul class="dropdown-menu" aria-labelledby="dropdown01">
                                    <li>
                                        <a class="dropdown-item"  href="{{ route('admin.admin-list') }}">{{ __('Profil') }}</a>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav> --}}



        
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark shadow" aria-label="Twelfth navbar example">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample10" aria-controls="navbarsExample10" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
      
                <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample10">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="#">GESTION SALARIALE</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="{{ route('admin.employee.employee-list') }}">Liste des salariers</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="{{ route('admin.pay.pay-history') }}">Historique de paiement</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-bs-toggle="dropdown" aria-expanded="false">{{ Auth::user()->nom }} {{ Auth::user()->prenom }}</a>
                            <ul class="dropdown-menu" aria-labelledby="dropdown01">
                                <li>
                                    <a class="dropdown-item"  href="{{ route('admin.admin-list') }}">{{ __('Liste des admnistrateurs') }}</a>
                                    <a class="dropdown-item text-center" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        {{ __('Deconnexion') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        {{-- <nav class="navbar navbar-expand-lg navbar-light bg-light rounded shadow" aria-label="Twelfth navbar example">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample10" aria-controls="navbarsExample10" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
      
                <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample10">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="#">GESTION SALARIALE</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="{{ route('admin.employee.employee-list') }}">Liste des salariers</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="{{ route('admin.pay.pay-history') }}">Historique de paiement</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-bs-toggle="dropdown" aria-expanded="false">{{ Auth::user()->nom }} {{ Auth::user()->prenom }}</a>
                            <ul class="dropdown-menu" aria-labelledby="dropdown01">
                                <li>
                                    <a class="dropdown-item"  href="{{ route('admin.admin-list') }}">{{ __('Liste des admnistrateurs') }}</a>
                                    <a class="dropdown-item text-center" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        {{ __('Deconnexion') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav> --}}

        <div class="table-responsive">
            <div class="text-center justify-content-center d-flex bg-body shadow">
                <a class="nav-link active" aria-current="page" href="{{ route('admin.acceuil') }}">Tableau de bord</a>
                {{-- <a class="nav-link" href="#">
                    Friends
                    <span class="badge bg-light text-dark rounded-pill align-text-bottom">27</span>
                </a> --}}
                <a class="nav-link" href="{{ route('admin.grille.salary-grille-list') }}">Grille salariale</a>
                <a class="nav-link" href="{{ route('admin.indemnite.indemnite-list') }}">Grille des indemnit&eacute;</a>
                <a class="nav-link" href="{{ route('admin.cotisation.cotisation-list') }}">Grille des cotisations</a>
                <a class="nav-link" href="{{ route('admin.echsal.echsal-list') }}">Salaire par echelon</a>
                <a class="nav-link" href="{{ route('admin.catsal.catsal-list') }}">Salaire par categorie</a>
            </div>
        </div>
        @endauth
@extends('layouts.app')

@section('content')
<div class="container">
    
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edition du Grille salariale') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('insert-into-indemnite') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="ech" class="col-md-4 col-form-label text-md-end">{{ __('Echelon') }}</label>

                            <div class="col-md-6">
                                <select id="ech" class="form-select @error('ech') is-invalid @enderror" name="ech" value="{{ $indemnites->ech }}" required autocomplete="ech" autofocus aria-label="Default select example">
                                    <option selected>choix ...</option>
                                    <option value="1">Echelon 1</option>
                                    <option value="2">Echelon 2</option>
                                    <option value="3">Echelon 3</option>
                                    <option value="4">Echelon 4</option>
                                    <option value="5">Echelon 5</option>
                                    <option value="6">Echelon 6</option>
                                    <option value="7">Echelon 7</option>
                                    <option value="8">Echelon 8</option>
                                    <option value="9">Echelon 9</option>
                                    <option value="10">Echelon 10</option>
                                    <option value="11">Echelon 11</option>
                                    <option value="12">Echelon 12</option>
                                    <option value="13">Echelon 13</option>
                                    <option value="14">Echelon 14</option>
                                    <option value="15">Echelon 15</option>
                                    <option value="16">Echelon 16</option>
                                </select>

                                @error('ech')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="row mb-3">
                            <label for="cat" class="col-md-4 col-form-label text-md-end">{{ __('Categorie') }}</label>

                            <div class="col-md-6">
                                <select id="cat" class="form-select @error('cat') is-invalid @enderror" name="cat" value="{{ $indemnites->cat }}" required autocomplete="cat" autofocus aria-label="Default select example">
                                <option selected>choix ...</option>
                                    <option value="1">Categorie 1</option>
                                    <option value="2">Categorie 2</option>
                                    <option value="3">Categorie 3</option>
                                    <option value="4">Categorie 4</option>
                                    <option value="5">Categorie 5</option>
                                    <option value="6">Categorie 6</option>
                                    <option value="7">Categorie 7</option>
                                    <option value="8">Categorie 8</option>
                                    <option value="9">Categorie 9</option>
                                    <option value="10">Categorie 10</option>
                                </select>

                                @error('cat')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="montant" class="col-md-4 col-form-label text-md-end">{{ __('Montant') }} de l&#039;indemint&eacute;</label>

                            <div class="col-md-6">
                                <input id="montant" type="number" class="form-control @error('montant') is-invalid @enderror" name="montant" value="{{ $indemnites->montant }}" required autocomplete="montant" autofocus>

                                @error('montant')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="desc" class="col-md-4 col-form-label text-md-end">{{ __('Description') }} de l&#039;indemint&eacute;</label>

                            <div class="col-md-6">
                                <input id="desc" type="text" class="form-control @error('desc') is-invalid @enderror" name="desc" value="{{ $indemnites->desc }}" required autocomplete="new-desc">

                                @error('desc')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Editer') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

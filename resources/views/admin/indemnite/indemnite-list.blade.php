@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mb-3">
        <div class="row mb-2">
            <div class="col-md-12">
                @if (session('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="row justify-content-center mb-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Grille indemnitaire') }}</div>

                <div class="card-body table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Echelons</th>
                                <th scope="col">Categories</th>
                                <th scope="col">Montant de l&#039;indemnit&eacute;</th>
                                <th scope="col">Description de l&#039;indemnit&eacute;</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($indemnites as $indemnite)
                            <tr>
                                <th scope="row">{{ $indemnite->id }}</th>
                                <td>{{ $indemnite->ech }}</td>
                                <td>{{ $indemnite->cat }}</td>
                                <td>{{ $indemnite->montant }}</td>
                                <td>{{ $indemnite->desc }}</td>
                                <td  class="btn-group">
                                    <a role="button" class="btn btn-secondary" href="{{ route('admin.form.edit.indemnite', $indemnite->id)}}">Modifier</a>
                                    <a role="button" class="btn btn-danger" href="{{ route('destroyindemnite', $indemnite->id)}}">Supprimer</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{-- Pagination --}}
                    <div class="d-flex justify-content-center">
                        {!! $indemnites->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mb-3">
        <a role="button" class="btn btn-primary" href="{{route('admin.indemnite.form-add-indemnite')}}">Ajouter</a>
    </div>

</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center mb-3">
        <div class="col-md-12">
                <div class="card-body">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <span>{{ __('Vous ')}}</span>
                        <span>&ecirc;</span><span>{{__('tes connecter!') }}</span>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center mb-3">
        <div class="col-md-12">
            <div class="card">
                <div class="d-flex card-header justify-content-between align-items-center">
                    <div>{{ __('Liste des administrateurs') }}</div>
                    <div class="text-info"><strong>{{ count($admins) }}</strong></div>
                </div>

                <div class="card-body table-responsive">
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nom</th>
                                <th scope="col">Prenom</th>
                                <th scope="col">Sexe</th>
                                <th scope="col">Email</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($admins) <= 10)
                                @foreach($admins as $admin)
                                <tr>
                                    <th scope="row">{{ $admin->id }}</th>
                                    <td>{{ $admin->nom }}</td>
                                    <td>{{ $admin->prenom }}</td>
                                    <td>{{ $admin->sexe }}</td>
                                    <td>{{ $admin->email }}</td>
                                    <td  class="w-100 btn-group">
                                        <a role="button" class="btn btn-primary" href="{{ route('admin.employee.employee-list') }}">Voire</a>
                                        <a role="button" class="btn btn-secondary mx-2" href="{{ route('admin.employee.employee-list') }}">Modifier</a>
                                        <a role="button" class="btn btn-danger" href="{{ route('admin.employee.employee-list') }}">Supprimer</a>
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                @for ($i = 0; $i < 11; $i++)
                                <tr>
                                    <th scope="row">{{ $admin->id }}</th>
                                    <td>{{ $admin->nom }}</td>
                                    <td>{{ $admin->prenom }}</td>
                                    <td>{{ $admin->sexe }}</td>
                                    <td>{{ $admin->email }}</td>
                                    <td  class="btn-group">
                                        <a role="button" class="btn btn-primary" href="#">Voire</a>
                                        <a role="button" class="btn btn-secondary" href="#">Modifier</a>
                                        <a role="button" class="btn btn-danger" href="#">Supprimer</a>
                                    </td>
                                </tr>
                                @endfor
                            @endif
                        </tbody>
                    </table>
                    <div class="mb-3">
                        <a role="button" class="btn btn-primary" href="{{ route('admin.admin-list') }}">Voire plus</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center mb-3">
        <div class="col-md-12">
            <div class="card">
                <div class="d-flex card-header justify-content-between align-items-center">
                    <div>{{ __('Liste des salariers') }}</div>
                    <div class="text-info"><strong>{{ count($employees) }}</strong></div>
                </div>

                <div class="card-body table-responsive">
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nom</th>
                                <th scope="col">Prenom</th>
                                <th scope="col">Sexe</th>
                                <th scope="col">Email</th>
                                <th scope="col">Type</th>
                                <th scope="col">Fonction</th>
                                <th scope="col">Echelon</th>
                                <th scope="col">Categorie</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($employees) <= 10)
                                @foreach($employees as $employee)
                                <tr>
                                    <th scope="row">{{ $employee->id }}</th>
                                    <td>{{ $employee->nom }}</td>
                                    <td>{{ $employee->prenom }}</td>
                                    <td>{{ $employee->sexe }}</td>
                                    <td>{{ $employee->email }}</td>
                                    <td>{{ $employee->type }}</td>
                                    <td>{{ $employee->intitule_fct }}</td>
                                    <td>{{ $employee->ech }}</td>
                                    <td>{{ $employee->cat }}</td>
                                    <td  class="w-100 btn-group">
                                        <a role="button" class="btn btn-primary" href="{{ route('admin.employee.employee-list') }}">Voire</a>
                                        <a role="button" class="btn btn-secondary mx-2" href="{{ route('admin.employee.employee-list') }}">Modifier</a>
                                        <a role="button" class="btn btn-danger" href="{{ route('admin.employee.employee-list') }}">Supprimer</a>
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                @for ($i = 0; $i < 11; $i++)
                                <tr>
                                    <th scope="row">{{ $employee->id }}</th>
                                    <td>{{ $employee->nom }}</td>
                                    <td>{{ $employee->prenom }}</td>
                                    <td>{{ $employee->sexe }}</td>
                                    <td>{{ $employee->email }}</td>
                                    <td>{{ $employee->type }}</td>
                                    <td>{{ $employee->intitule_fct }}</td>
                                    <td>{{ $employee->ech }}</td>
                                    <td>{{ $employee->cat }}</td>
                                    <td  class="btn-group">
                                        <a role="button" class="btn btn-primary" href="#">Voire</a>
                                        <a role="button" class="btn btn-secondary" href="#">Modifier</a>
                                        <a role="button" class="btn btn-danger" href="#">Supprimer</a>
                                    </td>
                                </tr>
                                @endfor
                            @endif
                        </tbody>
                    </table>
                    <div class="mb-3">
                        <a role="button" class="btn btn-primary" href="{{ route('admin.employee.employee-list') }}">Voire plus</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center mb-3">
        <div class="col-md-12">
            <div class="card">
                <div class="d-flex card-header justify-content-between align-items-center">
                    <div>{{ __('Historique de paiement') }}</div>
                    <div class="text-info"><strong>{{ count($history) }}</strong></div>
                </div>

                <div class="card-body table-responsive">
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">Users ID</th>
                                <th scope="col">Operateur</th>
                                <th scope="col">Numero de transaction</th>
                                <th scope="col">ID de transaction</th>
                                <th scope="col">Montant de la transaction</th>
                                <th scope="col">Date</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($history) <= 10)
                                @foreach($history as $historique)
                                <tr>
                                    <th scope="row">{{ $historique->users_id }}</th>
                                    <td>{{ $historique->operateur }}</td>
                                    <td>{{ $historique->num }}</td>
                                    <td>{{ $historique->IDTrans }}</td>
                                    <td>{{ $historique->montant }}</td>
                                    <td>{{ $historique->date }}</td>
                                    <td  class="w-100 btn-group">
                                        <a role="button" class="btn btn-primary" href="#">Voire</a>
                                        <a role="button" class="btn btn-secondary mx-2" href="#">Modifier</a>
                                        <a role="button" class="btn btn-danger" href="#">Supprimer</a>
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                @for ($i = 0; $i < 11; $i++)
                                <tr>
                                    <th scope="row">{{ $employee->users_id }}</th>
                                    <td>{{ $historique->operateur }}</td>
                                    <td>{{ $historique->num }}</td>
                                    <td>{{ $historique->IDTrans }}</td>
                                    <td>{{ $historique->montant }}</td>
                                    <td>{{ $historique->date }}</td>
                                    <td  class="btn-group">
                                        <a role="button" class="btn btn-primary" href="{{ route('admin.pay.pay-history') }}">Voire</a>
                                        <a role="button" class="btn btn-secondary" href="{{ route('admin.pay.pay-history') }}">Modifier</a>
                                        <a role="button" class="btn btn-danger" href="{{ route('admin.pay.pay-history') }}">Supprimer</a>
                                    </td>
                                </tr>
                                @endfor
                            @endif
                        </tbody>
                    </table>
                    <div class="mb-3">
                        <a role="button" class="btn btn-primary" href="{{ route('admin.pay.pay-history') }}">Voire plus</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center mb-3">
        <div class="col-md-12">
            <div class="card">
                <div class="d-flex card-header justify-content-between align-items-center">
                    <div>{{ __('Grille salariale') }}</div>
                    <div class="text-info"><strong>{{ count($grilles) }}</strong></div>
                </div>

                <div class="card-body table-responsive">
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Echelons</th>
                                <th scope="col">Categories</th>
                                <th scope="col">salaire de base</th>
                                <th scope="col">Poste</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($grilles) <= 10)
                                @foreach($grilles as $grille)
                                <tr>
                                    <th scope="row">{{ $grille->id }}</th>
                                    <td>{{ $grille->ech }}</td>
                                    <td>{{ $grille->cat }}</td>
                                    <td>{{ $grille->salbas }}</td>
                                    <td>{{ $grille->poste }}</td>
                                    <td  class="w-100 btn-group">
                                        <a role="button" class="btn btn-primary" href="#">Voire</a>
                                        <a role="button" class="btn btn-secondary" href="#">Modifier</a>
                                        <a role="button" class="btn btn-danger" href="#">Supprimer</a>
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                @for ($i = 0; $i < 11; $i++)
                                <tr>
                                    <th scope="row">{{ $grille->id }}</th>
                                    <td>{{ $grille->ech }}</td>
                                    <td>{{ $grille->cat }}</td>
                                    <td>{{ $grille->salbas }}</td>
                                    <td>{{ $grille->poste }}</td>
                                    <td  class="btn-group">
                                        <a role="button" class="btn btn-primary" href="#">Voire</a>
                                        <a role="button" class="btn btn-secondary" href="#">Modifier</a>
                                        <a role="button" class="btn btn-danger" href="#">Supprimer</a>
                                    </td>
                                </tr>
                                @endfor
                            @endif
                        </tbody>
                    </table>
                    <div class="mb-3">
                        <a role="button" class="btn btn-primary" href="{{ route('admin.grille.salary-grille-list') }}">Voire plus</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center mb-3">
        <div class="col-md-12">
            <div class="card">
                <div class="d-flex card-header justify-content-between align-items-center">
                    <div>{{ __('Grille des indemnit') }}&eacute;{{ __('s') }}</div>
                    <div class="text-info"><strong>{{ count($indemnites) }}</strong></div>
                </div>

                <div class="card-body table-responsive">
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Echelons</th>
                                <th scope="col">Categories</th>
                                <th scope="col">Montant d&#039;indemnit&eacute;</th>
                                <th scope="col">Description</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($indemnites) <= 10)
                                @foreach($indemnites as $indemnite)
                                <tr>
                                    <th scope="row">{{ $indemnite->id }}</th>
                                    <td>{{ $indemnite->ech }}</td>
                                    <td>{{ $indemnite->cat }}</td>
                                    <td>{{ $indemnite->montant }}</td>
                                    <td>{{ $indemnite->desc }}</td>
                                    <td  class="btn-group">
                                        <a role="button" class="btn btn-primary" href="#">Voire</a>
                                        <a role="button" class="btn btn-secondary" href="#">Modifier</a>
                                        <a role="button" class="btn btn-danger" href="#">Supprimer</a>
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                @for ($i = 0; $i < 11; $i++)
                                <tr>
                                    <th scope="row">{{ $grille->id }}</th>
                                    <td>{{ $indemnite->ech }}</td>
                                    <td>{{ $indemnite->cat }}</td>
                                    <td>{{ $indemnite->montant }}</td>
                                    <td>{{ $indemnite->desc }}</td>
                                    <td  class="btn-group">
                                        <a role="button" class="btn btn-primary" href="#">Voire</a>
                                        <a role="button" class="btn btn-secondary" href="#">Modifier</a>
                                        <a role="button" class="btn btn-danger" href="#">Supprimer</a>
                                    </td>
                                </tr>
                                @endfor
                            @endif
                        </tbody>
                    </table>
                    <div class="mb-3">
                        <a role="button" class="btn btn-primary" href="{{ route('admin.indemnite.indemnite-list') }}">Voire plus</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center mb-3">
        <div class="col-md-12">
            <div class="card">
                <div class="d-flex card-header justify-content-between align-items-center">
                    <div>{{ __('Grille des cotisations ou de retenus') }}</div>
                    <div class="text-info"><strong>{{ count($cotisations) }}</strong></div>
                </div>

                <div class="card-body table-responsive">
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Echelons</th>
                                <th scope="col">Categories</th>
                                <th scope="col">Montant d&#039;indemnit&eacute;</th>
                                <th scope="col">Description</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($indemnites) <= 10)
                                @foreach($cotisations as $cotisation)
                                <tr>
                                    <th scope="row">{{ $cotisation->id }}</th>
                                    <td>{{ $cotisation->ech }}</td>
                                    <td>{{ $cotisation->cat }}</td>
                                    <td>{{ $cotisation->montant }}</td>
                                    <td>{{ $cotisation->desc }}</td>
                                    <td  class="btn-group">
                                        <a role="button" class="btn btn-primary" href="#">Voire</a>
                                        <a role="button" class="btn btn-secondary" href="#">Modifier</a>
                                        <a role="button" class="btn btn-danger" href="#">Supprimer</a>
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                @for ($i = 0; $i < 11; $i++)
                                <tr>
                                    <th scope="row">{{ $cotisation->id }}</th>
                                    <td>{{ $cotisation->ech }}</td>
                                    <td>{{ $cotisation->cat }}</td>
                                    <td>{{ $cotisation->montant }}</td>
                                    <td>{{ $cotisation->desc }}</td>
                                    <td  class="btn-group">
                                        <a role="button" class="btn btn-primary" href="#">Voire</a>
                                        <a role="button" class="btn btn-secondary" href="#">Modifier</a>
                                        <a role="button" class="btn btn-danger" href="#">Supprimer</a>
                                    </td>
                                </tr>
                                @endfor
                            @endif
                        </tbody>
                    </table>
                    <div class="mb-3">
                        <a role="button" class="btn btn-primary" href="{{ route('admin.cotisation.cotisation-list') }}">Voire plus</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center mb-3">
        <div class="col-md-12">
            <div class="card">
                <div class="d-flex card-header justify-content-between align-items-center">
                    <div>{{ __('Grille salariale par echelons') }}</div>
                    <div class="text-info"><strong>{{ count($echsalaires) }}</strong></div>
                </div>

                <div class="card-body table-responsive">
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Echelons</th>
                                <th scope="col">Salaire de base</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($echsalaires) <= 10)
                                @foreach($echsalaires as $echsalaire)
                                <tr>
                                    <th scope="row">{{ $echsalaire->id }}</th>
                                    <td>{{ $echsalaire->ech }}</td>
                                    <td>{{ $echsalaire->salbas }}</td>
                                    <td  class="btn-group">
                                        <a role="button" class="btn btn-primary" href="#">Voire</a>
                                        <a role="button" class="btn btn-secondary" href="#">Modifier</a>
                                        <a role="button" class="btn btn-danger" href="#">Supprimer</a>
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                @for ($i = 0; $i < 11; $i++)
                                <tr>
                                    <th scope="row">{{ $echsalaire->id }}</th>
                                    <td>{{ $echsalaire->ech }}</td>
                                    <td>{{ $echsalaire->salbas }}</td>
                                    <td  class="btn-group">
                                        <a role="button" class="btn btn-primary" href="#">Voire</a>
                                        <a role="button" class="btn btn-secondary" href="#">Modifier</a>
                                        <a role="button" class="btn btn-danger" href="#">Supprimer</a>
                                    </td>
                                </tr>
                                @endfor
                            @endif
                        </tbody>
                    </table>
                    <div class="mb-3">
                        <a role="button" class="btn btn-primary" href="{{ route('admin.echsal.echsal-list') }}">Voire plus</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center mb-3">
        <div class="col-md-12">
            <div class="card">
                <div class="d-flex card-header justify-content-between align-items-center">
                    <div>{{ __('Grille salariale par categorie') }}</div>
                    <div class="text-info"><strong>{{ count($catsalaires) }}</strong></div>
                </div>

                <div class="card-body table-responsive">
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Echelons</th>
                                <th scope="col">Salaire de base</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($catsalaires) <= 10)
                                @foreach($catsalaires as $catsalaire)
                                <tr>
                                    <th scope="row">{{ $catsalaire->id }}</th>
                                    <td>{{ $catsalaire->ech }}</td>
                                    <td>{{ $catsalaire->salbas }}</td>
                                    <td  class="btn-group">
                                        <a role="button" class="btn btn-primary" href="#">Voire</a>
                                        <a role="button" class="btn btn-secondary" href="#">Modifier</a>
                                        <a role="button" class="btn btn-danger" href="#">Supprimer</a>
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                @for ($i = 0; $i < 11; $i++)
                                <tr>
                                    <th scope="row">{{ $catsalaire->id }}</th>
                                    <td>{{ $catsalaire->ech }}</td>
                                    <td>{{ $catsalaire->salbas }}</td>
                                    <td  class="btn-group">
                                        <a role="button" class="btn btn-primary" href="#">Voire</a>
                                        <a role="button" class="btn btn-secondary" href="#">Modifier</a>
                                        <a role="button" class="btn btn-danger" href="#">Supprimer</a>
                                    </td>
                                </tr>
                                @endfor
                            @endif
                        </tbody>
                    </table>
                    <div class="mb-3">
                        <a role="button" class="btn btn-primary" href="{{ route('admin.catsal.catsal-list') }}">Voire plus</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mb-3">
        <div class="row mb-2">
            <div class="col-md-12">
                @if (session('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="row justify-content-center mb-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Grille salariale') }}</div>

                <div class="card-body table-responsive">
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Echelons</th>
                                <th scope="col">Categories</th>
                                <th scope="col">Salaire de base</th>
                                <th scope="col">Poste cl&eacute;</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($grilles as $grille)
                            <tr>
                                <th scope="row">{{ $grille->id }}</th>
                                <td>{{ $grille->ech }}</td>
                                <td>{{ $grille->cat }}</td>
                                <td>{{ $grille->salbas }}</td>
                                <td>{{ $grille->poste }}</td>
                                <td  class="btn-group">
                                    <a role="button" class="btn btn-secondary" href="{{ route('admin.form.edit.grille', $grille->id)}}">Modifier</a>
                                    <a role="button" class="btn btn-danger" href="{{ route('destroygrille', $grille->id)}}">Supprimer</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{-- Pagination --}}
                    <div class="d-flex justify-content-center">
                        {!! $grilles->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mb-3">
        <a role="button" class="btn btn-primary" href="{{route('admin.grille.form-add-grille')}}">Ajouter</a>
    </div>

</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card shadow">
        <div class="card-header d-flex justify-content-between align-items-center">
            <div>{{ __('Liste des administrateurs') }}</div>
            <div class="text-info"><b>{{ count($admins) }}</b></div>
        </div>
        <div class="card-body table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="row">Nom</th>
                        <th scope="row">Prenom</th>
                        <th scope="row">Sexe</th>
                        <th scope="row">Email</th>
                        <th scope="row">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($admins as $admin)
                    <tr>
                        <th scope="col">{{$admin->nom}}</th>
                        <td>{{$admin->prenom}}</th>
                        @if($admin->sexe === 'M')
                            <td>Masculin</th>
                        @elseif($admin->sexe === 'F')
                            <td>Feminin</th>
                        @elseif($admin->sexe === 'R')
                            <td>Robot</th>
                        @else
                            <td>Autre</th>
                        @endif
                        <td>{{$admin->email}}</th>
                        <td class="btn-group">
                            <a class="btn btn-primary mx-1" href="{{ route('admin.form-edit-admin',$admin->id) }}">Modifier</a>
                            <a class="btn btn-danger mx-1" href="{{ route('destroyadmin', $admin->id) }}">Supprimer</a>
                        </th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{-- Pagination --}}
            <div class="d-flex justify-content-center">
                {!! $admins->links() !!}
            </div>
        </div>
    </div>
</div>
@endsection

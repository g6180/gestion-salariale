@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mb-3">
        <div class="row mb-2">
            <div class="col-md-12">
                @if (session('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="row justify-content-center mb-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Grille des cotisations') }}</div>

                <div class="card-body table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Echelons</th>
                                <th scope="col">Categories</th>
                                <th scope="col">Montant de l&#039;indemnit&eacute;</th>
                                <th scope="col">Description de l&#039;indemnit&eacute;</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($cotisations as $cotisation)
                            <tr>
                                <th scope="row">{{ $cotisation->id }}</th>
                                <td>{{ $cotisation->ech }}</td>
                                <td>{{ $cotisation->cat }}</td>
                                <td>{{ $cotisation->montant }}</td>
                                <td>{{ $cotisation->desc }}</td>
                                <td  class="btn-group">
                                    <a role="button" class="w-100 btn btn-secondary" href="{{ route('admin.form.edit.cotisation', $cotisation->id)}}">Modifier</a>
                                    <a role="button" class="w-100btn btn-danger" href="{{ route('destroycotisation', $cotisation->id)}}">Supprimer</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{-- Pagination --}}
                    <div class="d-flex justify-content-center">
                        {!! $cotisations->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mb-3">
        <a role="button" class="btn btn-primary" href="{{route('admin.cotisation.form-add-cotisation')}}">Ajouter</a>
    </div>

</div>
@endsection

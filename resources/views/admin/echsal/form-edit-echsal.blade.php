@extends('layouts.app')

@section('content')
<div class="container">
    
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edition du Grille salariale par categorie') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('update.echsal', $echsalaires->id) }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="ech" class="col-md-4 col-form-label text-md-end">{{ __('Echelon') }}</label>

                            <div class="col-md-6">
                                <select id="ech" class="form-select @error('ech') is-invalid @enderror" name="ech" value="{{ $echsalaires->ech }}" required autocomplete="ech" autofocus aria-label="Default select example">
                                    <option selected>choix ...</option>
                                    <option value="1">Echelon 1</option>
                                    <option value="2">Echelon 2</option>
                                    <option value="3">Echelon 3</option>
                                    <option value="4">Echelon 4</option>
                                    <option value="5">Echelon 5</option>
                                    <option value="6">Echelon 6</option>
                                    <option value="7">Echelon 7</option>
                                    <option value="8">Echelon 8</option>
                                    <option value="9">Echelon 9</option>
                                    <option value="10">Echelon 10</option>
                                    <option value="11">Echelon 11</option>
                                    <option value="12">Echelon 12</option>
                                    <option value="13">Echelon 13</option>
                                    <option value="14">Echelon 14</option>
                                    <option value="15">Echelon 15</option>
                                    <option value="16">Echelon 16</option>
                                </select>

                                @error('ech')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="row mb-3">
                            <label for="salbas" class="col-md-4 col-form-label text-md-end">{{ __('Salaire de base') }}</label>

                            <div class="col-md-6">
                                <input id="salbas" type="number" class="form-control @error('salbas') is-invalid @enderror" name="salbas" value="{{ $echsalaires->salbas }}" required autocomplete="salbas" autofocus>

                                @error('salbas')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Editer') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mb-3">
        <div class="row mb-2">
            <div class="col-md-12">
                @if (session('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="row justify-content-center mb-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Grille salariale par echelon') }}</div>

                <div class="card-body table-responsive">
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Echelons</th>
                                <th scope="col">Salaire de base</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($echsalaires as $echsalaire)
                            <tr>
                                <th scope="row">{{ $echsalaire->id }}</th>
                                <td>{{ $echsalaire->ech }}</td>
                                <td>{{ $echsalaire->salbas }}</td>
                                <td  class="btn-group">
                                    <a role="button" class="btn btn-secondary" href="{{ route('admin.form.edit.echsal', $echsalaire->id)}}">Modifier</a>
                                    <a role="button" class="btn btn-danger" href="{{ route('destroyechsal', $echsalaire->id)}}">Supprimer</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{-- Pagination --}}
                    <div class="d-flex justify-content-center">
                        {!! $echsalaires->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mb-3">
        <a role="button" class="btn btn-primary" href="{{route('admin.echsal.form-add-echsal')}}">Ajouter</a>
    </div>

</div>
@endsection

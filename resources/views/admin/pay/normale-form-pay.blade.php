@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center mb-3">
        <div class="col-md-8">
            <div class="bg-body rounded shadow">
                <div class="card-body text-center">
                    <span>{{ __('Paiement de salaire de : ') }}</span>
                    <span class="text-info text-bolder">{{ $employees->nom }} {{ $employees->prenom }}</span>
                    <span class="text-info text-bolder">{{ $employees->intitule_fct }}</span>
                </div>
            </div>
        </div>
    </div>

    <form method="POST" action="{{ route('insert-pay', $employees->id) }}">
        @csrf
        <div class="row justify-content-center mb-3">
            <div class="col-md-8">
                <div class="bg-body rounded shadow">
                    <div class="card-body">

                            <div class="mb-3">
                                <div class="col-sm-12">
                                    <input type="hidden" class="form-control" id="inputPassword" placeholder="User id" value="{{ $employees->id }}" name="users_id" required/>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="type" class="form-label">Type de paiement</label>
                                {{-- <input class="form-control" type="text" value="Avance sur salaire" name="type" disabled> --}}
                                <select id="type" class="form-select @error('type') is-invalid @enderror" name="type" value="{{ old('type') }}" required autocomplete="type" autofocus required>
                                    <option selected>choix ...</option>
                                    <option value="Normale">Salaire normale</option>
                                </select>
                                @error('type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="mode" class="form-label">Mode de paiement</label>
                                <select id="mode" class="form-select @error('mode') is-invalid @enderror" name="mode" value="{{ old('mode') }}" required autocomplete="mode" autofocus required>
                                    <option selected>choix ...</option>
                                    <option value="Espece">&Eacute;spece</option>
                                    <option value="Cheque">Ch&eacute;que</option>
                                    <option value="Virement Electronique">Virement Electronique</option>
                                    <option value="virement Bancaire">virement Bancaire</option>
                                </select>

                                @error('mode')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="inputPassword" class="col-sm-5 col-form-label">Nom de l&#039;operateur</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="inputPassword" placeholder="Nom de l operateur" name="operateur" equired />
                                </div>
                            </div>

                            <div class="mb-3">
                                <label for="inputPassword" class="col-sm-5 col-form-label">Numero du paiement</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="inputPassword" placeholder="Numero de transacion" name="num" required/>
                                </div>
                            </div>

                            <div class="mb-3">
                                <label for="inputPassword" class="col-sm-5 col-form-label">ID du paiement</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="inputPassword" placeholder="ID de la transaction" name="IDTrans" required />
                                </div>
                            </div>

                            <div class="mb-3">
                                <label for="inputPassword" class="col-sm-5 col-form-label">Montant du paiement</label>
                                <div class="col-sm-12">
                                    <input type="number" class="form-control" id="inputPassword" placeholder="Montant" value="00112" name="montant" required="" disabled>
                                </div>
                                <div class="email-help"><small>fgrdgrggg</small></div>
                            </div>

                            <div class="mb-3">
                                <label for="inputPassword" class="col-sm-5 col-form-label">Date de paiement</label>
                                <div class="col-sm-12">
                                    <input type="date" class="form-control" id="inputPassword" placeholder="Date de paiement" name="date" required/>
                                </div>
                            </div>

                        {{-- <div class="mb-3">
                            <input type="submit"  class="btn btn-primary" value="payer"></button>
                        </div> --}}
                    {{-- </form> --}}
                </div>
            </div>
        </div>
    </div>

        <div class="row justify-content-center mb-3">
            <div class="col-md-8">
                <div class="bg-body rounded shadow">
                    <button type="submit" class="w-100 btn btn-primary">PAYER</button>
                </div>
            </div>
        </div>
    </form>
    
   
</div>
@endsection

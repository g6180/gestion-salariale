@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center mb-3">
            
            <div class="col-md-12 mb-3">
                <div class="card shadow-sm">
                    <div class="d-flex card-header justify-content-between align-items-center">
                        <div>{{ __('Liste des transactions de ') }}</div>
                        <div class="text-info"><strong>{{count($historiques)}}</strong></div>
                    </div>

                    <div class="card-body table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="row">ID</th>
                                    <th scope="row">ID Users</th>
                                    <th scope="row">Mode du transfert</th>
                                    <th scope="row">Montant du transfert</th>
                                    <th scope="row">Numero de du transfert</th>
                                    <th scope="row">ID du transfert</th>
                                    <th scope="row">Date du transfert</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($historiques as $historique)
                                <tr>
                                    <th scope="col">{{$historique->id}}</th>
                                    <td>{{$historique->users_id}}</td>
                                    <td>{{$historique->mode}}</td>
                                    <td>{{$historique->montant}}</td>
                                    <td>{{$historique->num}}</td>
                                    <td>{{$historique->IDTrans}}</td>
                                    <td>{{$historique->date}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{-- Pagination --}}
                        <div class="d-flex justify-content-center">
                            {!! $employees->links() !!}
                        </div>
                    </div>
                </div>
            </div>


        </div>


        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
            
        </div>

            

@endsection

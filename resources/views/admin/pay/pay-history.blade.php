@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row justify-content-center mb-3">
            <div class="row mb-2">
                <div class="col-md-12">
                    @if (session('status'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        
        <div class="row justify-content-center mb-3">
            <div class="col-md-12 mb-3">
                <div class="card shadow-sm">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div>{{ __('Historique de paiement' ) }} (<span class="text-info"><strong>{{ count($history) }}</strong></span>)</div>
                        {{-- <div class="input-group">
                            <input type="text" class="form-control" placeholder="Promo code">
                            <button type="submit" class="btn btn-secondary">Redeem</button>
                        </div> --}}
                        <form class="d-flex form-inline my-2 my-lg-0" action="{{ route('admin.pay.pay-search') }}" method="GET" role="search">
                            {{ csrf_field() }}
                            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" name="q">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Go</button>
                        </form>
                    </div>

                    <div class="card-body table-responsive">
                        <table class="table table-sm table-striped">
                            <thead>
                                <tr>
                                    <th scope="row">Identi&eacute;s</th>
                                    <th scope="row">sexe</th>
                                    <th scope="row">Email</th>
                                    <th scope="row">Type</th>
                                    <th scope="row">Mode</th>
                                    <th scope="row">Operateur</th>
                                    <th scope="row">N.Trans</th>
                                    <th scope="row">ID.Trans</th>
                                    <th scope="row">Montant</th>
                                    <th scope="row">Solde</th>
                                    <th scope="row">Date</th>
                                    <th scope="row">Status</th>
                                    <th scope="row">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($employees as $employee)
                                <tr>
                                    <th scope="col">{{ $employee->nom }} {{ $employee->prenom }}</th>
                                    <td>{{ $employee->sexe }}</td>
                                    <td>{{ $employee->email }}</td>
                                    <td>{{ $employee->type }}</td>
                                    <td>{{ $employee->mode }}</td>
                                    <td>{{ $employee->operateur }}</td>
                                    <td>{{ $employee->num }}</td>
                                    <td>{{ $employee->IDTrans }}</td>
                                    <td>{{ $employee->montant }}</td>
                                    <td>{{ $employee->solde }}</td>
                                    <td>{{ $employee->date }}</td>
                                    <td>
                                        <a class="btn btn-success disabled" tabindex="-1"
                                        ><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check2-all" viewBox="0 0 16 16">
                                            <path d="M12.354 4.354a.5.5 0 0 0-.708-.708L5 10.293 1.854 7.146a.5.5 0 1 0-.708.708l3.5 3.5a.5.5 0 0 0 .708 0l7-7zm-4.208 7-.896-.897.707-.707.543.543 6.646-6.647a.5.5 0 0 1 .708.708l-7 7a.5.5 0 0 1-.708 0z"/>
                                            <path d="m5.354 7.146.896.897-.707.707-.897-.896a.5.5 0 1 1 .708-.708z"/>
                                        </svg></a>
                                    </td>
                                    <td class="w-100 btn-group">
                                        <a class="btn btn-primary mx-1" href="{{url("/admin/pay/pay-invoice/{$employee->users_id}/{$employee->date}")}}"> Bulletin </a>
                                        {{-- <a  href="{{url('admin/pay/pay-invoice//')}}"  target="_blank" >Bulletin</a> --}}
                                        <a class="btn btn-secondary"  href="{{ route('admin.pay.employeesPayHistoryList',$employee->users_id) }}">Details</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{-- Pagination --}}
                        <div class="d-flex justify-content-center">
                            {!! $history->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection

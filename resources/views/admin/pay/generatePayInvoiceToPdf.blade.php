<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Fiche de paie</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link href="{{ asset('css/payinvoice.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="container bootdey">
		<div class="row justify-content-center">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="widget-box">
					@foreach ($salariers as $salarier)
						<div class="widget-header widget-header-large">
							<h3 class="widget-title grey lighter"><i class="ace-icon fa fa-leaf green"></i>FICHE DE PAIE</h3>
							<div class="widget-toolbar no-border invoice-info">
								<span class="invoice-info-label">Facture:</span><span class="red">#{{$salarier->id}}</span>
								<br>
								<span class="invoice-info-label">Date:</span><span class="blue">{{$salarier->date}}</span>
							</div>
							<div class="widget-toolbar hidden-480"><a href="#"><i class="ace-icon fa fa-print"></i></a></div>
						</div>

						<div class="widget-body">
							<div class="widget-main padding-24">
								<div class="row">
									<div class="col-sm-6">
										<div class="row">
											<div class="col-xs-11 label label-lg label-info arrowed-in arrowed-right"><b>Information de l&#039;entreprise</b></div>
										</div>

										<div>
											<ul class="list-unstyled spaced">
												<li><i class="ace-icon fa fa-caret-right blue"></i>Burkina Faso</li>
												<li><i class="ace-icon fa fa-caret-right blue"></i>Essitech International</li>
												<li><i class="ace-icon fa fa-caret-right blue"></i>01 BP 6622, Ouagadougou 01</li>
												<li><i class="ace-icon fa fa-caret-right blue"></i>Telephone:<b class="red">111-111-111</b></li>
												<li class="divider"></li>
												<li><i class="ace-icon fa fa-caret-right blue"></i>Information du paiement</li>
											</ul>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="row">
											<div class="col-xs-11 label label-lg label-success arrowed-in arrowed-right"><b>Information du salari&eacute;</b></div>
										</div>

										<div>
											<ul class="list-unstyled  spaced">
												<li><i class="ace-icon fa fa-caret-right green"></i>{{$salarier->nom}} {{$salarier->prenom}}</li>
												<li><i class="ace-icon fa fa-caret-right green"></i>{{$salarier->email}}</li>
												<li><i class="ace-icon fa fa-caret-right green"></i>{{$salarier->intitule_fct}}</li>
												<li class="divider"></li>
												<li><i class="ace-icon fa fa-caret-right green"></i>{{$salarier->tel}}</li>
											</ul>
										</div>
									</div>
								</div>

								<div class="space"></div>

								<div>
									<table class="table table-striped table-bordered">
										<thead>
											<tr>
												<th class="center">#</th>
												<th>Revenus</th>
												<th class="hidden-xs">Montant</th>
												<th class="hidden-480">Deductions</th>
												<th>Montant</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="center">1</td>
												<td>Salaire de base</td>
												<td class="hidden-xs">
													<a href="#">{{$salarier->montant}}</a>
												</td>
												<td class="hidden-480">UITS</td>
												<td></td>
											</tr>
											<tr>
												<td class="center">2</td>
												<td>IF</td>
												<td class="hidden-xs"></td>
												<td class="hidden-480">IR</td>
												<td></td>
											</tr>
											<tr>
												<td class="center">3</td>
												<td>IT</td>
												<td class="hidden-xs"> --- </td>
												<td class="hidden-480">Autres</td>
												<td></td>
											</tr>
											<tr>
												<td class="center">4</td>
												<td>IL</td>
												<td class="hidden-xs"> --- </td>
												<td class="hidden-480"> --- </td>
												<td> --- </td>
											</tr>
											<tr>
												<td class="center"></td>
												<td>Total Gains</td>
												<td class="hidden-xs">
													<a href="#">{{$salarier->montant}}</a>
												</td>
												<td class="hidden-480">Total des d&eacute;ductions</td>
												<td><a href="#">000</a></td>
											</tr>
										</tbody>
									</table>
								</div>

								<div class="hr hr8 hr-double hr-dotted"></div>

								<div class="row">
									<div class="col-sm-5 pull-right">
										<h4 class="pull-right">Salaire net &aacute; payer :<span class="red">{{$salarier->montant}}</span></h4>
									</div>
									<div class="col-sm-7 pull-left">
										<span>Mode de paiement : {{$salarier->mode}}</span>
									</div>
								</div>

								<div class="space-6"></div>
									<div class="well text-center">A CONSERVER SANS LIMITATION DE DUREE</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>

		<script src="{{ asset('js/app.js') }}" type="text/js"></script>
	</body>
</html>
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1 class="my-4">R&egrave;sultat de recherche pour: <small class="bg-warning">{{$key}}</small></h1>
            <div class="card mb-3">
                <div class="card-body table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                {{-- <th scope="col">Nom</th>
                                <th scope="col">Prenom</th>
                                <th scope="col">Sexe</th>
                                <th scope="col">Email</th> --}}
                                <th scope="col">Mode</th>
                                <th scope="col">Operateur</th>
                                <th scope="col">Num.Trans</th>
                                <th scope="col">ID.Trans</th>
                                <th scope="col">Montant</th>
                                <th scope="col">Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($employees as $employee)
                            <tr>
                                <th scope="row">{{$loop->iteration}}</th>
                                {{-- <td>
                                    @if(strpos($employee->nom,$key) !== false) 
                                       <span class="bg-warning">{{$employee->nom}}</span>
                                    @else
                                        <span>{{$employee->nom}}</span>
                                    @endif
                                </td>
                                <td>
                                    @if(strpos($employee->prenom,$key) !== false) 
                                       <span class="bg-warning">{{$employee->prenom}}</span>
                                    @else
                                        <span>{{$employee->prenom}}</span>
                                    @endif
                                </td>
                                <td>
                                    @if(strpos($employee->sexe,$key) !== false) 
                                       <span class="bg-warning">{{$employee->sexe}}</span>
                                    @else
                                        <span>{{$employee->sexe}}</span>
                                    @endif
                                </td>
                                <td>
                                    @if(strpos($employee->email,$key) !== false) 
                                       <span class="bg-warning">{{$employee->email}}</span>
                                    @else
                                        <span>{{$employee->email}}</span>
                                    @endif
                                </td> --}}
                                <td>
                                    @if(strpos($employee->mode,$key) !== false) 
                                       <span class="bg-warning">{{$employee->mode}}</span>
                                    @else
                                        <span>{{$employee->mode}}</span>
                                    @endif
                                </td>
                                <td>
                                    @if(strpos($employee->operateur,$key) !== false) 
                                       <span class="bg-warning">{{$employee->operateur}}</span>
                                    @else
                                        <span>{{$employee->operateur}}</span>
                                    @endif
                                </td>
                                <td>
                                    @if(strpos($employee->num,$key) !== false) 
                                       <span class="bg-warning">{{$employee->num}}</span>
                                    @else
                                        <span>{{$employee->num}}</span>
                                    @endif
                                </td>
                                <td>
                                    @if(strpos($employee->IDTrans,$key) !== false) 
                                       <span class="bg-warning">{{$employee->IDTrans}}</span>
                                    @else
                                        <span>{{$employee->IDTrans}}</span>
                                    @endif
                                </td>
                                <td>
                                    @if(strpos($employee->montant,$key) !== false) 
                                       <span class="bg-warning">{{$employee->montant}}</span>
                                    @else
                                        <span>{{$employee->montant}}</span>
                                    @endif
                                </td>
                                <td>
                                    @if(strpos($employee->date,$key) !== false) 
                                       <span class="bg-warning">{{$employee->date}}</span>
                                    @else
                                        <span>{{$employee->date}}</span>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection

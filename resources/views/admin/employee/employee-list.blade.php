@extends('layouts.app')

@section('content')
<div class="container">


    <div class="mb-3 d-flex flex-row-reverse">
        <a role="button" class="btn btn-primary" href="{{route('admin.form-add-employee')}}">Ajouter</a>
    </div>

    <div class="row justify-content-center mb-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <div>{{ __('Liste des salariers') }}</div>
                    <form class="d-flex form-inline my-2 my-lg-0" action="{{ route('admin.employee.employee-search') }}" method="GET" role="search">
                        {{ csrf_field() }}
                        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" name="q">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Go</button>
                    </form>
                </div>

                <div class="card-body table-responsive">
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Nom</th>
                                <th scope="col">Prenom</th>
                                <th scope="col">Sexe</th>
                                <th scope="col">Email</th>
                                <th scope="col">Telephone</th>
                                <th scope="col">N.Securite_sociale</th>
                                <th scope="col">Fonction</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($employees as $employee)
                                <tr>
                                    <th scope="row">{{ $employee->id }}</th>
                                    <td>{{ $employee->nom }}</td>
                                    <td>{{ $employee->prenom }}</td>
                                    <td>{{ $employee->sexe }}</td>
                                    <td>{{ $employee->email }}</td>
                                    <td>{{ $employee->tel }}</td>
                                    <td>{{ $employee->nss }}</td>
                                    <td>{{ $employee->intitule_fct }}</td>
                                    <td  class="btn-group">
                                        <a role="button" class="w-100 btn btn-primary" href="#" data-bs-toggle="modal"
                                                data-bs-target="#voireplus">Voire plus</a>
                                        <!-- Modal -->
                                        <div class="modal fade" id="voireplus" tabindex="-1" aria-labelledby="detailsLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title text-center" id="detailsLabel">
                                                            <span>{{ __('Plus d') }}&#039;nformation sur <span>
                                                            <span class="text-info">{{ $employee->nom }} {{ $employee->prenom }}</span>
                                                            <span>{{ __('fonctionaire') }}</span>
                                                            <span class="text-info">( {{ $employee->intitule_fct }} )</span>
                                                            <span>{{ __('de categorie  ') }}</span>
                                                            <span class="text-info">{{ $employee->cat }}</span>
                                                            <span>{{ __('et d') }}</span>
                                                            <span>&#039;echelon <span>
                                                            <span class="text-info">{{ $employee->ech }}</span>
                                                        </h5>
                                                        
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="d-flex justify-content-between align-items-center mb-3">
                                                            <div>Nom :</div>
                                                            <div>{{ $employee->nom }}</div>
                                                        </div>
                                                        <div class="d-flex justify-content-between align-items-center mb-3">
                                                            <div>Prenom :</div>
                                                            <div>{{ $employee->prenom }}</div>
                                                        </div>
                                                        <div class="d-flex justify-content-between align-items-center mb-3">
                                                            <div>Sexe :</div>
                                                            <div>{{ $employee->sexe }}</div>
                                                        </div>
                                                        <div class="d-flex justify-content-between align-items-center mb-3">
                                                            <div>Email :</div>
                                                            <div>{{ $employee->email }}</div>
                                                        </div>
                                                        <div class="d-flex justify-content-between align-items-center mb-3">
                                                            <div>Date de naissance :</div>
                                                            <div>{{ $employee->date_naiss }}</div>
                                                        </div>
                                                        <div class="d-flex justify-content-between align-items-center mb-3">
                                                            <div>Lieu de naissance :</div>
                                                            <div>{{ $employee->lieu_naiss }}</div>
                                                        </div>
                                                        <div class="d-flex justify-content-between align-items-center mb-3">
                                                            <div>Date de recrutement :</div>
                                                            <div>{{ $employee->date_rec }}</div>
                                                        </div>
                                                        <div class="d-flex justify-content-between align-items-center mb-3">
                                                            <div>Lieu de recrutement :</div>
                                                            <div>{{ $employee->lieu_rec }}</div>
                                                        </div>
                                                        <div class="d-flex justify-content-between align-items-center mb-3">
                                                            <div>Address :</div>
                                                            <div>{{ $employee->address }}</div>
                                                        </div>
                                                        <div class="d-flex justify-content-between align-items-center mb-3">
                                                            <div>Num&eacute;ro de T&eacute;lephone principale :</div>
                                                            <div>{{ $employee->tel }}</div>
                                                        </div>
                                                        <div class="d-flex justify-content-between align-items-center mb-3">
                                                            <div>Intitul&eacute; de fonction :</div>
                                                            <div>{{ $employee->intitule_fct }}</div>
                                                        </div>
                                                        <div class="d-flex justify-content-between align-items-center mb-3">
                                                            <div>Ech&eacute;lon :</div>
                                                            <div>{{ $employee->ech }}</div>
                                                        </div>
                                                        <div class="d-flex justify-content-between align-items-center mb-3">
                                                            <div>Cat&eacute;gorie :</div>
                                                            <div>{{ $employee->cat }}</div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="w-100 btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                                                        {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <a role="button" class="btn btn-warning" href="{{ route('admin.pay.pay-type', $employee->id)}}">Payer</a>
                                        <a role="button" class="btn btn-secondary" href="{{ route('admin.form.edit.employee', $employee->id)}}">Modifier</a>
                                        <a role="button" class="btn btn-danger" href="{{ route('destroyemploy', $employee->id)}}">Supprimer</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{-- Pagination --}}
                    <div class="d-flex justify-content-center">
                        {!! $employees->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    

</div>
@endsection

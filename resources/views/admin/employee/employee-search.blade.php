@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1 class="my-4">R&egrave;sultat de recherche pour: <small class="bg-warning">{{$key}}</small></h1>
            <div class="card mb-3">
                <div class="card-body table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nom</th>
                                <th scope="col">Prenom</th>
                                <th scope="col">Sexe</th>
                                <th scope="col">Email</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($salariers as $salarier)
                            <tr>
                                <th scope="row">{{$loop->iteration}}</th>
                                <td>
                                    @if(strpos($salarier->nom,$key) !== false) 
                                       <span class="bg-warning">{{$salarier->nom}}</span>
                                    @else
                                        <span>{{$salarier->nom}}</span>
                                    @endif
                                </td>
                                <td>
                                    @if(strpos($salarier->prenom,$key) !== false) 
                                       <span class="bg-warning">{{$salarier->prenom}}</span>
                                    @else
                                        <span>{{$salarier->prenom}}</span>
                                    @endif
                                </td>
                                <td>
                                    @if(strpos($salarier->sexe,$key) !== false) 
                                       <span class="bg-warning">{{$salarier->sexe}}</span>
                                    @else
                                        <span>{{$salarier->sexe}}</span>
                                    @endif
                                </td>
                                <td>
                                    @if(strpos($salarier->email,$key) !== false) 
                                       <span class="bg-warning">{{$salarier->email}}</span>
                                    @else
                                        <span>{{$salarier->email}}</span>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            {{-- <div class="card">
                <div class="card-header">categories</div>
                <div class="card-body">
                    {{$categories}}
                </div>
            </div>

            <div class="card">
                <div class="card-header">tag</div>
                <div class="card-body">
                    {{$tags}}
                </div>
            </div> --}}

            <div class="card mb-3">
                <div class="card-header">Recent posts</div>
                <div class="card-body">
                    {{$recent_posts}}
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection

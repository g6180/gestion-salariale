@extends('layouts.app')

@section('content')

<div class="row justify-content-center mt-5">
    <div class="col-md-3 mt-5">
        <div class="card border-secondary">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="card-header h3 mb-3 fw-normal text-center">Connectez vous svp !!!</div>
                <div class="card-body form-signin">
                    <div class="form-floating">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="name@example.com" />
                        <label for="email" class="form-label">{{ __('Address mail') }}</label>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-floating">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Mot de pass">
                        <label for="password" class="form-label">{{ __('Mot de pass') }}</label>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-6 offset-md-4">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
        
                            <label class="form-check-label" for="remember">
                                {{ __('Rester connectez') }}
                            </label>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button class="w-100 btn btn-lg btn-primary" type="submit">Connexion</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="nom" class="col-md-4 col-form-label text-md-end">{{ __('Nom') }}</label>

                            <div class="col-md-6">
                                <input id="nom" type="text" class="form-control @error('nom') is-invalid @enderror" name="nom" value="{{ old('nom') }}" required autocomplete="nom" autofocus>

                                @error('nom')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="prenom" class="col-md-4 col-form-label text-md-end">{{ __('Prenom') }}</label>

                            <div class="col-md-6">
                                <input id="prenom" type="text" class="form-control @error('prenom') is-invalid @enderror" name="prenom" value="{{ old('prenom') }}" required autocomplete="prenom" autofocus>

                                @error('prenom')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="sexe" class="col-md-4 col-form-label text-md-end">{{ __('Sexe') }}</label>

                            <div class="col-md-6">
                                <select id="sexe" class="form-select @error('sexe') is-invalid @enderror" aria-label="Default select example" name="sexe" value="{{ old('sexe') }}" required autocomplete="sexem" autofocus>
                                    <option selected>Choix</option>
                                    <option value="M">Masculin</option>
                                    <option value="F">Feminin</option>
                                    <option value="R">Robot</option>
                                    <option value="A">Autre</option>
                                </select>
                                @error('sexe')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <!-- <div class="row mb-3">
                            <label for="date_naiss" class="col-md-4 col-form-label text-md-end">{{ __('Date de naissance') }}</label>

                            <div class="col-md-6">
                                <input id="date_naiss" type="date" class="form-control @error('date_naiss') is-invalid @enderror" name="date_naiss" value="{{ old('date_naiss') }}" required autocomplete="date_naiss">

                                @error('date_naiss')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="lieu_naiss" class="col-md-4 col-form-label text-md-end">{{ __('Lieu de naissance') }}</label>

                            <div class="col-md-6">
                                <input id="lieu_naiss" type="text" class="form-control @error('lieu_naiss') is-invalid @enderror" name="lieu_naiss" value="{{ old('lieu_naiss') }}" required autocomplete="lieu_naiss">

                                @error('lieu_naiss')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="date_rec" class="col-md-4 col-form-label text-md-end">{{ __('Date de recrutement') }}</label>

                            <div class="col-md-6">
                                <input id="date_rec" type="date" class="form-control @error('date_rec') is-invalid @enderror" name="date_rec" value="{{ old('date_rec') }}" required autocomplete="date_rec">

                                @error('date_rec')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="lieu_rec" class="col-md-4 col-form-label text-md-end">{{ __('Lieu de recrutement') }}</label>

                            <div class="col-md-6">
                                <input id="lieu_rec" type="text" class="form-control @error('lieu_rec') is-invalid @enderror" name="lieu_rec" value="{{ old('lieu_rec') }}" required autocomplete="lieu_rec">

                                @error('lieu_rec')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="address" class="col-md-4 col-form-label text-md-end">{{ __('Address') }}</label>

                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required autocomplete="address">

                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="tel1" class="col-md-4 col-form-label text-md-end">{{ __('Numero de Telephone principale') }}</label>

                            <div class="col-md-6">
                                <input id="tel1" type="text" class="form-control @error('tel1') is-invalid @enderror" name="tel1" value="{{ old('tel1') }}" required autocomplete="tel1">

                                @error('tel1')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="tel2" class="col-md-4 col-form-label text-md-end">{{ __('Numero de Telephone secondaire') }}</label>

                            <div class="col-md-6">
                                <input id="tel2" type="text" class="form-control @error('tel2') is-invalid @enderror" name="tel2" value="{{ old('tel2') }}" required autocomplete="tel2">

                                @error('tel2')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="intule_fct" class="col-md-4 col-form-label text-md-end">{{ __('Intitule fonction') }}</label>

                            <div class="col-md-6">
                                <select id="intitule_fct" class="form-select @error('intitule_fct') is-invalid @enderror" name="intitule_fct" value="{{ old('intitule_fct') }}" required autocomplete="intitule_fct" autofocus aria-label="Default select example">
                                    <option selected>choix ...</option>
                                    <option value="Fonction 1">Fonction 1</option>
                                    <option value="Fonction 2">Fonction 2</option>
                                    <option value="3">Fonction 3</option>
                                    <option value="Fonction 3">Fonction 4</option>
                                    <option value="Fonction 5">Fonction 5</option>
                                    <option value="Fonction 6">Fonction 6</option>
                                    <option value="Fonction 7">Fonction 7</option>
                                    <option value="Fonction 8">Fonction 8</option>
                                </select>

                                @error('intule_fct')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="ech" class="col-md-4 col-form-label text-md-end">{{ __('Echelon') }}</label>

                            <div class="col-md-6">
                                <select id="ech" class="form-select @error('ech') is-invalid @enderror" name="ech" value="{{ old('ech') }}" required autocomplete="ech" autofocus aria-label="Default select example">
                                    <option selected>choix ...</option>
                                    <option value="1">Echelon 1</option>
                                    <option value="2">Echelon 2</option>
                                    <option value="3">Echelon 3</option>
                                    <option value="4">Echelon 4</option>
                                    <option value="5">Echelon 5</option>
                                    <option value="6">Echelon 6</option>
                                    <option value="7">Echelon 7</option>
                                    <option value="8">Echelon 8</option>
                                    <option value="9">Echelon 9</option>
                                    <option value="10">Echelon 10</option>
                                    <option value="11">Echelon 11</option>
                                    <option value="12">Echelon 12</option>
                                    <option value="13">Echelon 13</option>
                                    <option value="14">Echelon 14</option>
                                    <option value="15">Echelon 15</option>
                                    <option value="16">Echelon 16</option>
                                </select>

                                @error('ech')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> -->
                        
                        <!-- <div class="row mb-3">
                            <label for="cat" class="col-md-4 col-form-label text-md-end">{{ __('Categorie') }}</label>

                            <div class="col-md-6">
                                <select id="cat" class="form-select @error('cat') is-invalid @enderror" name="cat" value="{{ old('cat') }}" required autocomplete="cat" autofocus aria-label="Default select example">
                                <option selected>choix ...</option>
                                    <option value="1">Categorie 1</option>
                                    <option value="2">Categorie 2</option>
                                    <option value="3">Categorie 3</option>
                                    <option value="4">Categorie 4</option>
                                    <option value="5">Categorie 5</option>
                                    <option value="6">Categorie 6</option>
                                    <option value="7">Categorie 7</option>
                                    <option value="8">Categorie 8</option>
                                    <option value="9">Categorie 9</option>
                                    <option value="10">Categorie 10</option>
                                </select>

                                @error('cat')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> -->

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-end">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

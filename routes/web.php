<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\history;
use App\Models\Grille;
use App\Models\Indemnite;
use App\Models\Cotisation;
use App\Http\Controllers\Fpdf;
use App\Models\CatSal;
use App\Models\EchSal;
use app\Models\Salarier;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::permanentRedirect('/home', '/admin/employee-list');

// route for admin dashbord
Route::middleware(['auth', 'type:admin'])->group(function () {
    // Route::permanentRedirect('/', '/admin/acceuil');
    Route::get('/admin/acceuil', [App\Http\Controllers\AdminController::class, 'index'])->name('admin.acceuil');
  
    Route::get('admin/admin-list', [App\Http\Controllers\AdminController::class, 'showAdminList'])->name('admin.admin-list');

    Route::get('admin/form-edit-admin/{id}', [App\Http\Controllers\AdminController::class, 'edit'])->name('admin.form-edit-admin');
    Route::put('update-admin/{id}', [App\Http\Controllers\AdminController::class, 'update'])->name('admin-update');

    Route::delete('admin/delete/{id}', [App\Http\Controllers\AdminController::class, 'destroy'])->name('destroyadmin');

});


// routes for employee
Route::middleware(['auth', 'type:admin'])->group(function () {
    Route::get('admin/employee-list', [App\Http\Controllers\SalarierController::class, 'index'])->name('admin.employee.employee-list');

    Route::get('admin/employee/employee-search', [App\Http\Controllers\SalarierController::class, 'search'])->name('admin.employee.employee-search');

    Route::get('admin/form-add-employee', [App\Http\Controllers\SalarierController::class, 'create'])->name('admin.form-add-employee');
    Route::post('insert-employee', [App\Http\Controllers\SalarierController::class, 'insert'])->name('insert-employee');

    Route::get('admin/form-edit-employee/{id}', [App\Http\Controllers\SalarierController::class, 'edit'])->name('admin.form.edit.employee');
    Route::put('update-employee/{id}', [App\Http\Controllers\SalarierController::class, 'update'])->name('update.employee');

    Route::get('/delete/employ{id}', [App\Http\Controllers\SalarierController::class, 'destroy'])->name('destroyemploy');

});


// Route pour historique de paiement
Route::middleware(['auth', 'type:admin'])->group(function () { // middleware(['auth', 'type:admin']
    Route::get('admin/pay/pay-type/{id}', [App\Http\Controllers\HistoryController::class, 'payType'])->name('admin.pay.pay-type');

    // Route::post('normal-pay-store', [App\Http\Controllers\HistoryController::class, 'normalePay'])->name('normal-pay-store');
    Route::get('admin/pay/normale-form-pay/{id}', [App\Http\Controllers\HistoryController::class, 'normalePay'])->name('admin.pay.normale-form-pay');
    // Route::post('insert-normale-pay', [App\Http\Controllers\HistoryController::class, 'store'])->name('insert-normale-pay');


    
    Route::get('admin/pay/advance-form-pay/{id}', [App\Http\Controllers\HistoryController::class, 'advancePay'])->name('admin.pay.advance-form-pay');
    Route::post('insert-pay', [App\Http\Controllers\HistoryController::class, 'PayStore'])->name('insert-pay');






    
    Route::get('admin/pay/pay-invoice/{id}/{date}', [App\Http\Controllers\HistoryController::class, 'payinvoice'])->name('admin.pay.pay-invoice');

    Route::get('admin/pay/pay-history', [App\Http\Controllers\HistoryController::class, 'index'])->name('admin.pay.pay-history');

    Route::get('admin/pay/pay-search', [App\Http\Controllers\HistoryController::class, 'search'])->name('admin.pay.pay-search');

    Route::get('admin/pay/employeesPayHistoryList/{id}', [App\Http\Controllers\HistoryController::class, 'employeesPayHistoryList'])->name('admin.pay.employeesPayHistoryList');

    Route::get('admin/pay/generatePayInvoiceToPdf/{id}/{date}', [App\Http\Controllers\HistoryController::class, 'generatePayInvoiceToPdf'])->name('admin.pay.generatePayInvoiceToPdf');
});


 // Route pour Grille salaiale
Route::middleware(['auth', 'type:admin'])->group(function () { // middleware(['auth', 'type:admin']

    Route::get('admin/grille/salary-grille-list', function () {
        $grilles = Grille::get();
        $grilles = Grille::paginate(9);
        return view('admin.grille.salary-grille-list', compact('grilles'));
    })->name('admin.grille.salary-grille-list');

    Route::get('admin/grille/form-add-grille', [App\Http\Controllers\GrilleController::class, 'create'])->name('admin.grille.form-add-grille');
    Route::post('insert-into-grille', [App\Http\Controllers\GrilleController::class, 'store'])->name('insert-into-grille');

    Route::get('admin/form-edit-grille/{id}', [App\Http\Controllers\GrilleController::class, 'edit'])->name('admin.form.edit.grille');
    Route::put('update-grille/{id}', [App\Http\Controllers\GrilleController::class, 'update'])->name('update.grille');

    Route::get('/delete/{id}', [App\Http\Controllers\GrilleController::class, 'destroy'])->name('destroygrille');


});

// Route pour Grille indemnitaire
Route::middleware(['auth', 'type:admin'])->group(function () { // middleware(['auth', 'type:admin']
    Route::get('admin/indemnite/indemnite-list', function () {
        $indemnites = Indemnite::get();
        $indemnites = Indemnite::paginate(9);
        return view('admin.indemnite.indemnite-list', compact('indemnites'));
    })->name('admin.indemnite.indemnite-list');

    Route::get('admin/indemnite/form-add-indemnite', [App\Http\Controllers\IndemniteController::class, 'create'])->name('admin.indemnite.form-add-indemnite');
    Route::post('insert-into-indemnite', [App\Http\Controllers\IndemniteController::class, 'store'])->name('insert-into-indemnite');

    Route::get('admin/form-edit-indemnite/{id}', [App\Http\Controllers\IndemniteController::class, 'edit'])->name('admin.form.edit.indemnite');
    Route::put('update-indemnite/{id}', [App\Http\Controllers\IndemniteController::class, 'update'])->name('update.indemnite');

    Route::get('/delete/indemnite/{id}', [App\Http\Controllers\IndemniteController::class, 'destroy'])->name('destroyindemnite');

});

// Route pour Grille de cotisation
Route::middleware(['auth', 'type:admin'])->group(function () { // middleware(['auth', 'type:admin']
    Route::get('admin/cotisation/cotisation-list', function () {
        $cotisations = Cotisation::get();
        $cotisations = Cotisation::paginate(9);
        return view('admin.cotisation.cotisation-list', compact('cotisations'));
    })->name('admin.cotisation.cotisation-list');

    Route::get('admin/cotisation/form-add-cotisation', [App\Http\Controllers\CotisationController::class, 'create'])->name('admin.cotisation.form-add-cotisation');
    Route::post('insert-into-cotisation', [App\Http\Controllers\CotisationController::class, 'store'])->name('insert-into-cotisation');

    Route::get('admin/form-edit-cotisation/{id}', [App\Http\Controllers\CotisationController::class, 'edit'])->name('admin.form.edit.cotisation');
    Route::put('update-cotisation/{id}', [App\Http\Controllers\CotisationController::class, 'update'])->name('update.cotisation');

    Route::get('/delete/cotisation/{id}', [App\Http\Controllers\CotisationController::class, 'destroy'])->name('destroycotisation');

});

// Route pour Grille salariale par echelons
Route::middleware(['auth', 'type:admin'])->group(function () { // middleware(['auth', 'type:admin']
    Route::get('admin/echsal/echsal-list', function () {
        $echsalaires = EchSal::get();
        $echsalaires = EchSal::paginate(9);
        return view('admin.echsal.echsal-list', compact('echsalaires'));
    })->name('admin.echsal.echsal-list');

    Route::get('admin/echsal/form-add-echsal', [App\Http\Controllers\EchSalController::class, 'create'])->name('admin.echsal.form-add-echsal');
    Route::post('insert-into-echsal', [App\Http\Controllers\EchSalController::class, 'store'])->name('insert-into-echsal');

    Route::get('admin/form-edit-echsal/{id}', [App\Http\Controllers\EchSalController::class, 'edit'])->name('admin.form.edit.echsal');
    Route::post('update-echsal/{id}', [App\Http\Controllers\EchSalController::class, 'update'])->name('update.echsal');

    Route::get('/delete/echsal/{id}', [App\Http\Controllers\EchSalController::class, 'destroy'])->name('destroyechsal');

});

// Route pour Grille salariale par categorie
Route::middleware(['auth', 'type:admin'])->group(function () { // middleware(['auth', 'type:admin']
    Route::get('admin/catsal/catsal-list', function () {
        $catsalaires = CatSal::get();
        $catsalaires = CatSal::paginate(9);
        return view('admin.catsal.catsal-list', compact('catsalaires'));
    })->name('admin.catsal.catsal-list');

    Route::get('admin/catsal/form-add-catsal', [App\Http\Controllers\CatSalController::class, 'create'])->name('admin.catsal.form-add-catsal');
    Route::post('insert-into-catsal', [App\Http\Controllers\CatSalController::class, 'store'])->name('insert-into-catsal');

    Route::get('admin/form-edit-catsal/{id}', [App\Http\Controllers\CatSalController::class, 'edit'])->name('admin.form.edit.catsal');
    Route::post('update-catsal/{id}', [App\Http\Controllers\CatSalController::class, 'update'])->name('update.catsal');

    Route::get('/delete/catsal/{id}', [App\Http\Controllers\CatSalController::class, 'destroy'])->name('destroycatsal');

});



Route::get('pdf', [App\Http\Controllers\PdfController::class, 'generateToPdfWithFpdf']);

Route::get('htmlToPdf', [App\Http\Controllers\HTMLPDFController::class, 'htmlPdf'])->name('htmlToPdf');

Route::get('simple-qrcode', [App\Http\Controllers\SimpleQRcodeController::class, 'generate'])->name('invoice');



// <a class="btn btn-primary" href="{{ URL::to('/employee/pdf') }}">Export to PDF</a>
Route::get('generate-invoice-pdf', [App\Http\Controllers\PDFController::class, 'show']);
